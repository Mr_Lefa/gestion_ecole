



                                            										

/*
 * Java transformer for entity table inscription 
 * Created on 2021-01-18 ( Time 17:45:56 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.smile.ecole.dao.entity.Inscription;
import ci.smile.ecole.dao.repository.*;
import ci.smile.ecole.utils.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.*;
import ci.smile.ecole.utils.dto.transformer.*;
import ci.smile.ecole.utils.enums.*;
import ci.smile.ecole.dao.entity.Classe;
import ci.smile.ecole.dao.entity.Eleve;
import ci.smile.ecole.dao.entity.Annee;
import ci.smile.ecole.dao.entity.*;

/**
BUSINESS for table "inscription"
 * 
 * @author Geo
 *
 */
@Log
@Component
public class InscriptionBusiness implements IBasicBusiness<Request<InscriptionDto>, Response<InscriptionDto>> {

	private Response<InscriptionDto> response;
	@Autowired
	private InscriptionRepository inscriptionRepository;
//	@Autowired
//	private UserRepository userRepository;
	@Autowired
	private ClasseRepository classeRepository;
	@Autowired
	private NoteRepository noteRepository;
	@Autowired
	private EleveRepository eleveRepository;
	@Autowired
	private AnneeRepository anneeRepository;

//    @Autowired
//    private UserBusiness userBusiness;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

	public InscriptionBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	
	/**
	 * create Inscription by using InscriptionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<InscriptionDto> create(Request<InscriptionDto> request, Locale locale)  {
		log.info("----begin create Inscription-----");
		
		response = new Response<InscriptionDto>();
		
		try {
//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.CREATE_INSCRIPTION.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}
			
			List<Inscription> items = new ArrayList<Inscription>();
			
			for (InscriptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("eleveId", dto.getEleveId());
				fieldsToVerify.put("anneeId", dto.getAnneeId());
				fieldsToVerify.put("classeId", dto.getClasseId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if inscription to insert do not exist
				Inscription existingEntity = null;
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("inscription id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if classe exist
				Classe existingClasse = null;
				if (dto.getClasseId() != null && dto.getClasseId() > 0){
					existingClasse = classeRepository.findOne(dto.getClasseId(), false);
					if (existingClasse == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("classe classeId -> " + dto.getClasseId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				// Verify if eleve exist
				Eleve existingEleve = null;
				if (dto.getEleveId() != null && dto.getEleveId() > 0){
					existingEleve = eleveRepository.findOne(dto.getEleveId(), false);
					if (existingEleve == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("eleve eleveId -> " + dto.getEleveId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				// Verify if annee exist
				Annee existingAnnee = null;
				if (dto.getAnneeId() != null && dto.getAnneeId() > 0){
					existingAnnee = anneeRepository.findOne(dto.getAnneeId(), false);
					if (existingAnnee == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("annee anneeId -> " + dto.getAnneeId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				Inscription entityToSave = null;
				entityToSave = InscriptionTransformer.INSTANCE.toEntity(dto, existingClasse, existingEleve, existingAnnee);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Inscription> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = inscriptionRepository.saveAll((Iterable<Inscription>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("inscription", locale));
					response.setHasError(true);
					return response;
				}
				List<InscriptionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? InscriptionTransformer.INSTANCE.toLiteDtos(itemsSaved) : InscriptionTransformer.INSTANCE.toDtos(itemsSaved);
				
				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			log.info("----end create Inscription-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Inscription by using InscriptionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<InscriptionDto> update(Request<InscriptionDto> request, Locale locale)  {
		log.info("----begin update Inscription-----");
		
		response = new Response<InscriptionDto>();
		
		try {
//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.UPDATE_INSCRIPTION.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}

			List<Inscription> items = new ArrayList<Inscription>();
			
			for (InscriptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la inscription existe
				Inscription entityToSave = null;
				entityToSave = inscriptionRepository.findOne(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("inscription id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if classe exist
				if (dto.getClasseId() != null && dto.getClasseId() > 0){
					Classe existingClasse = classeRepository.findOne(dto.getClasseId(), false);
					if (existingClasse == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("classe classeId -> " + dto.getClasseId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setClasse(existingClasse);
				}
				// Verify if eleve exist
				if (dto.getEleveId() != null && dto.getEleveId() > 0){
					Eleve existingEleve = eleveRepository.findOne(dto.getEleveId(), false);
					if (existingEleve == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("eleve eleveId -> " + dto.getEleveId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setEleve(existingEleve);
				}
				// Verify if annee exist
				if (dto.getAnneeId() != null && dto.getAnneeId() > 0){
					Annee existingAnnee = anneeRepository.findOne(dto.getAnneeId(), false);
					if (existingAnnee == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("annee anneeId -> " + dto.getAnneeId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setAnnee(existingAnnee);
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Inscription> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = inscriptionRepository.saveAll((Iterable<Inscription>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("inscription", locale));
					response.setHasError(true);
					return response;
				}
				List<InscriptionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? InscriptionTransformer.INSTANCE.toLiteDtos(itemsSaved) : InscriptionTransformer.INSTANCE.toDtos(itemsSaved);

				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			log.info("----end update Inscription-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete Inscription by using InscriptionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<InscriptionDto> delete(Request<InscriptionDto> request, Locale locale)  {
		log.info("----begin delete Inscription-----");
		
		response = new Response<InscriptionDto>();
		
		try {

//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.DELETE_INSCRIPTION.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}

			List<Inscription> items = new ArrayList<Inscription>();
			
			for (InscriptionDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la inscription existe
				Inscription existingEntity = null;
				existingEntity = inscriptionRepository.findOne(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("inscription -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// note
				List<Note> listOfNote = noteRepository.findByInscriptionId(existingEntity.getId(), false);
				if (listOfNote != null && !listOfNote.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNote.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				inscriptionRepository.saveAll((Iterable<Inscription>) items);

				response.setHasError(false);
			}

			log.info("----end delete Inscription-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get Inscription by using InscriptionDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<InscriptionDto> getByCriteria(Request<InscriptionDto> request, Locale locale) {
		log.info("----begin get Inscription-----");
		
		response = new Response<InscriptionDto>();
		
		try {
//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}
//
//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.VIEW_INSCRIPTION.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}

			List<Inscription> items = null;
			items = inscriptionRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<InscriptionDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? InscriptionTransformer.INSTANCE.toLiteDtos(items) : InscriptionTransformer.INSTANCE.toDtos(items);

				final int size = items.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);
				response.setCount(inscriptionRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("inscription", locale));
				response.setHasError(false);
				return response;
			}

			log.info("----end get Inscription-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full InscriptionDto by using Inscription as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private InscriptionDto getFullInfos(InscriptionDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

}
