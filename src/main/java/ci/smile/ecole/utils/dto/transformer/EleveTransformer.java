

/*
 * Java transformer for entity table eleve 
 * Created on 2021-01-15 ( Time 15:14:45 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.utils.dto.transformer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.smile.ecole.dao.entity.Eleve;
import ci.smile.ecole.utils.contract.FullTransformerQualifier;
import ci.smile.ecole.utils.dto.EleveDto;


/**
 * TRANSFORMER for table "eleve"
 * 
 * @author Geo
 *
 */
@Mapper
public interface EleveTransformer {

	EleveTransformer INSTANCE = Mappers.getMapper(EleveTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.dateDeNaissance", dateFormat="dd/MM/yyyy",target="dateDeNaissance"),
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
	})
	EleveDto toDto(Eleve entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<EleveDto> toDtos(List<Eleve> entities) throws ParseException;

    default EleveDto toLiteDto(Eleve entity) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		if (entity == null) {
			return null;
		}
		EleveDto dto = new EleveDto();
		dto.setId( entity.getId() );
		dto.setNom( entity.getNom() );
		dto.setPrenom( entity.getPrenom() );
		dto.setDateDeNaissance(simpleDateFormat.format(entity.getDateDeNaissance()));
		return dto;
    }

	default List<EleveDto> toLiteDtos(List<Eleve> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<EleveDto> dtos = new ArrayList<EleveDto>();
		for (Eleve entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.matricule", target="matricule"),
		@Mapping(source="dto.nom", target="nom"),
		@Mapping(source="dto.prenom", target="prenom"),
		@Mapping(source="dto.sexe", target="sexe"),
		@Mapping(source="dto.dateDeNaissance",dateFormat="dd/MM/yyyy", target="dateDeNaissance"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    Eleve toEntity(EleveDto dto) throws ParseException;

    //List<Eleve> toEntities(List<EleveDto> dtos) throws ParseException;

}
