/*
 * Created on 2021-01-18 ( Time 17:46:18 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.utils.contract;


import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import org.mapstruct.Qualifier;

/**
 * Request Base
 * 
 * @author Geo
 *
 */
@Qualifier
@Target(ElementType.METHOD)
public @interface FullTransformerQualifier {

}
