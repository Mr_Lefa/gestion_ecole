package ci.smile.ecole.utils.report;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ci.smile.ecole.utils.ParamsUtils;
import ci.smile.ecole.utils.dto.EleveDto;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@Component
public class Report {

	@Autowired
	private ParamsUtils paramsUtils;
	
	private JasperPrint jasperPrint;
	private String defaultUserReportUrl = "reports/user.jrxml";
	private String defaultProfilReportUrl = "reports/profil.jrxml";
	private String defaultListClientsReportUrl = "reports/clients.jrxml";
	private String defaultListPrestatairesReportUrl = "reports/prestataires.jrxml";
	private String defaultProfilsReportUrl = "reports/profils.jrxml";
	private String defaultInvitationReportUrl = "reports/Email_A4.jrxml";
	private String defaultListDemandesReportUrl = "reports/demandes.jrxml";
	
	private String defaultListEleveReportUrl = "reports/elevecomplete.jrxml";
	
	
	private SimpleDateFormat dateFormat;

	JRXlsxExporter exporter = new JRXlsxExporter();
	public Report() {
		dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
	}

//	public String getUserReportLite(UserDto data) throws JRException, FileNotFoundException {
//		String reportLocation = "";
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		List<Object> dataSource = new ArrayList<Object>();
//		dataSource.add(data);
//		//ReportUtils.ajoutBase64ImagesScs(data);
//		jasperPrint = ReportUtils.getJasperPrint(defaultUserReportUrl, parameters, dataSource);
//		reportLocation = getReportLocation("User", "_" + data.getName(), "pdf");
//		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(reportLocation));
//		return reportLocation;
//	}
//	
//	public String getXlsUserReportLite(UserDto data) throws JRException, FileNotFoundException {
//		String reportLocation = "";
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		List<Object> dataSource = new ArrayList<Object>();
//		dataSource.add(data);
//		//ReportUtils.ajoutBase64ImagesScs(data);
//		jasperPrint = ReportUtils.getJasperPrint(defaultUserReportUrl, parameters, dataSource);
//		reportLocation = getReportLocation("User", "_" + data.getName(), "xlsx");
//		//JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(reportLocation));
//		JRXlsxExporter exporter = new JRXlsxExporter(); // initialize exporter 
//	    exporter.setExporterInput(new SimpleExporterInput(jasperPrint)); // set compiled report as input
//	    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reportLocation));  // set output file via path with filename
//	    SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
//	    configuration.setOnePagePerSheet(true); // setup configuration
//	    configuration.setDetectCellType(true);
//	    exporter.setConfiguration(configuration); // set configuration
//	    exporter.exportReport();
//		return reportLocation;
//	}
//	
//	public String getProfilReportLite(ProfilDto data) throws JRException, FileNotFoundException {
//		String reportLocation = "";
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		List<Object> dataSource = new ArrayList<Object>();
//		dataSource.add(data);
//		//ReportUtils.ajoutBase64ImagesScs(data);
//		jasperPrint = ReportUtils.getJasperPrint(defaultProfilReportUrl, parameters, dataSource);
//		reportLocation = getReportLocation("Profil", "_" + data.getName(), "pdf");
//		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(reportLocation));
//		return reportLocation;
//	}
//
//	public String getInvitationReportLite(UserDto data) throws JRException, FileNotFoundException {
//		String reportLocation = "";
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		List<Object> dataSource = new ArrayList<Object>();
//		dataSource.add(data);
//		jasperPrint = ReportUtils.getJasperPrint(defaultInvitationReportUrl, parameters, dataSource);
//		reportLocation = getReportLocation("User", "_" + data.getEmail(), "pdf");
//		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(reportLocation));
//		return reportLocation;
//	}

//	private String getReportLocation(String reportTemplateName, String dataCode, String fileExtension) {
//		//String filesDirectory = "/tmp/";
//		System.out.println(paramsUtils.getJasperDirectory());
//		String filesDirectory = paramsUtils.getJasperDirectory();
////		String pathServer = Utilities.getSuitableFileDirectory("liste.pdf", paramsUtils);
////		String filesDirectory = Utilities.saveFile("liste" + dateFormat.format(new Date()) , "." , "pdf", paramsUtils);
//		
//		//response.setFilePathDoc(Utilities.getFileUrlLink(fileName, paramsUtils));
//		//Utilities.createDirectory(filesDirectory);
////		if (!filesDirectory.endsWith("/"))
////			filesDirectory += "/";
//		reportTemplateName = (reportTemplateName != null) ? reportTemplateName + "_" : "";
//
//		return filesDirectory + reportTemplateName + dataCode + "_"
//				+ dateFormat.format(PicturesUtilities.getCurrentDate()) + "." + fileExtension;
//	}
	
	private String getReportLocation(String reportTemplateName, String dataCode, String fileExtension) {
	    String filesDirectory = "/home/siaka/";
	    PicturesUtilities.createDirectory(filesDirectory);
	    if (!filesDirectory.endsWith("/"))
	     filesDirectory += "/";
	    reportTemplateName = (reportTemplateName != null) ? reportTemplateName + "_" : "";

	    return filesDirectory + reportTemplateName + dataCode + "_"
	      + dateFormat.format(PicturesUtilities.getCurrentDate()) + "." + fileExtension;
	   }

	
	
//	public String getListClientsReportLite(List<UserDto> data) throws JRException, FileNotFoundException {
//
//		String reportLocation = "";
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		parameters.put("Parameter1", new JRBeanCollectionDataSource(data));
//		List<Object> dataSource = new ArrayList<Object>();
//		dataSource.add(data);
//		//ReportUtils.ajoutBase64ImagesScs(data);
//		jasperPrint = ReportUtils.getJasperPrint(defaultListClientsReportUrl, parameters, dataSource);
//		reportLocation = getReportLocation("User", "_" + data.get(0).getName(), "pdf");
//		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(reportLocation));
//		String [] getFilename = reportLocation.split("/");
//		return Utilities.getSuitableJasperUrl(getFilename[5], paramsUtils);
//	}
//	
//	public String getListPrestatairesReportLite(List<UserDto> data) throws JRException, FileNotFoundException {
//		String filesDirectory = paramsUtils.getJasperDirectory();
//		String date = dateFormat.format(PicturesUtilities.getCurrentDate());
//		String reportLocation = filesDirectory + "User" + data.get(0).getName() + "_"
//				+ date + "." + "pdf";
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		parameters.put("Parameter1", new JRBeanCollectionDataSource(data));
//		List<Object> dataSource = new ArrayList<Object>();
//		dataSource.add(data);
//		//ReportUtils.ajoutBase64ImagesScs(data);
//		jasperPrint = ReportUtils.getJasperPrint(defaultListPrestatairesReportUrl, parameters, dataSource);
//		//reportLocation = getReportLocation("User", "_" + data.get(0).getName(), "pdf");
//		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(reportLocation));
//		return paramsUtils.getJasperLink() + "files/" + "User" + data.get(0).getName() + "_"
//				+ date + "." + "pdf";
//	}
//	public String getListDemandesReportLite(List<ServiceDto> data) throws JRException, FileNotFoundException {
//		String reportLocation = "";
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		parameters.put("Parameter1", new JRBeanCollectionDataSource(data));
//		List<Object> dataSource = new ArrayList<Object>();
//		dataSource.addAll(data);
//		//ReportUtils.ajoutBase64ImagesScs(data);
//		jasperPrint = ReportUtils.getJasperPrint(defaultListDemandesReportUrl, parameters, dataSource);
//		reportLocation = getReportLocation("Demandes", "_" + data.get(0).getId(), "pdf");
//		JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(reportLocation));
//		String [] getFilename = reportLocation.split("/");
//		return Utilities.getSuitableJasperUrl(getFilename[5], paramsUtils);
//	}
//	
//	public String getxlsListUsersReportLite(List<UserDto> data) throws JRException, FileNotFoundException {
//		String reportLocation = "";
//		Map<String, Object> parameters = new HashMap<String, Object>();
//		parameters.put("Parameter1", new JRBeanCollectionDataSource(data));
//		List<Object> dataSource = new ArrayList<Object>();
//		dataSource.add(data);
//		//ReportUtils.ajoutBase64ImagesScs(data);
//		jasperPrint = ReportUtils.getJasperPrint(defaultListClientsReportUrl, parameters, dataSource);
//		reportLocation = getReportLocation("User", "_" + data.get(0).getName(), "xlsx");
//		//JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(reportLocation));
//		JRXlsxExporter exporter = new JRXlsxExporter(); // initialize exporter 
//	    exporter.setExporterInput(new SimpleExporterInput(jasperPrint)); // set compiled report as input
//	    exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(reportLocation));  // set output file via path with filename
//	    SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
//	    configuration.setOnePagePerSheet(true); // setup configuration
//	    configuration.setDetectCellType(true);
//	    exporter.setConfiguration(configuration); // set configuration
//	    exporter.exportReport();
//		return reportLocation;
//	}
	public String getListElevesReportLite(List<EleveDto> data) throws JRException, FileNotFoundException {
		 String reportLocation = "";
		 Map<String, Object> parameters = new HashMap<String, Object>();
		 parameters.put("ParameterEleve", new JRBeanCollectionDataSource(data));
		 List<Object> dataSource = new ArrayList<Object>();
		 dataSource.addAll(data);
		 //ReportUtils.ajoutBase64ImagesScs(data);
		 jasperPrint = ReportUtils.getJasperPrint(defaultListEleveReportUrl, parameters, dataSource);
		 reportLocation = getReportLocation("eleves", "_" + data.get(0).getId(), "pdf");
		 JasperExportManager.exportReportToPdfStream(jasperPrint, new FileOutputStream(reportLocation));
		 String [] getFilename = reportLocation.split("/");
		 return reportLocation;
		}

}
