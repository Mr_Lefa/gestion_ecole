package ci.smile.ecole.utils.report;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;

public class PicturesUtilities {

	
	public static Date getCurrentDate() {
		return new Date();
	}

	public static String convertToBase64(String pathFichier) {
	    File file = new File(Thread.currentThread().getContextClassLoader().getResource(pathFichier).getFile());
	    return getStringImage(file);
	}
	
	
	
	
	public static String convertToBase64ForTest(String pathFichier) {
	    File file = new File(pathFichier);
	    return getStringImage(file);
	}
	
	
	
	private static String getStringImage(File file) {
	    try {
	      FileInputStream fin = new FileInputStream(file);
	      byte[] imageBytes = new byte[(int) file.length()];
	      fin.read(imageBytes, 0, imageBytes.length);
	      fin.close();
	      return org.apache.tomcat.util.codec.binary.Base64.encodeBase64String(imageBytes);
	    } catch (Exception e) {
	      e.printStackTrace();
	    }
	    return null;
	  }
	
	public static void createDirectory(String chemin) {
		File file = new File(chemin);
		if (!file.exists()) {
			try {
				FileUtils.forceMkdir(file);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}


}
