package ci.smile.ecole.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.smile.ecole.dao.entity.*;
import ci.smile.ecole.dao.repository.customize._InscriptionRepository;
import ci.smile.ecole.utils.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.*;

/**
 * Repository : Inscription.
 */
@Repository
public interface InscriptionRepository extends JpaRepository<Inscription, Integer>, _InscriptionRepository {
	/**
	 * Finds Inscription by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Inscription whose id is equals to the given id. If
	 *         no Inscription is found, this method returns null.
	 */
	@Query("select e from Inscription e where e.id = :id and e.isDeleted = :isDeleted")
	Inscription findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Inscription by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Inscription whose createdAt is equals to the given createdAt. If
	 *         no Inscription is found, this method returns null.
	 */
	@Query("select e from Inscription e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Inscription> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Inscription by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Inscription whose updatedAt is equals to the given updatedAt. If
	 *         no Inscription is found, this method returns null.
	 */
	@Query("select e from Inscription e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Inscription> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Inscription by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Inscription whose createdBy is equals to the given createdBy. If
	 *         no Inscription is found, this method returns null.
	 */
	@Query("select e from Inscription e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Inscription> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Inscription by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Inscription whose updatedBy is equals to the given updatedBy. If
	 *         no Inscription is found, this method returns null.
	 */
	@Query("select e from Inscription e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Inscription> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Inscription by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Inscription whose isDeleted is equals to the given isDeleted. If
	 *         no Inscription is found, this method returns null.
	 */
	@Query("select e from Inscription e where e.isDeleted = :isDeleted")
	List<Inscription> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Inscription by using classeId as a search criteria.
	 * 
	 * @param classeId
	 * @return An Object Inscription whose classeId is equals to the given classeId. If
	 *         no Inscription is found, this method returns null.
	 */
	@Query("select e from Inscription e where e.classe.id = :classeId and e.isDeleted = :isDeleted")
	List<Inscription> findByClasseId(@Param("classeId")Integer classeId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Inscription by using eleveId as a search criteria.
	 * 
	 * @param eleveId
	 * @return An Object Inscription whose eleveId is equals to the given eleveId. If
	 *         no Inscription is found, this method returns null.
	 */
	@Query("select e from Inscription e where e.eleve.id = :eleveId and e.isDeleted = :isDeleted")
	List<Inscription> findByEleveId(@Param("eleveId")Integer eleveId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Inscription by using anneeId as a search criteria.
	 * 
	 * @param anneeId
	 * @return An Object Inscription whose anneeId is equals to the given anneeId. If
	 *         no Inscription is found, this method returns null.
	 */
	@Query("select e from Inscription e where e.annee.id = :anneeId and e.isDeleted = :isDeleted")
	List<Inscription> findByAnneeId(@Param("anneeId")Integer anneeId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of Inscription by using inscriptionDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Inscription
	 * @throws DataAccessException,ParseException
	 */
	default List<Inscription> getByCriteria(Request<InscriptionDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Inscription e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Inscription> query = em.createQuery(req, Inscription.class);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Inscription by using inscriptionDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Inscription
	 * 
	 */
	default Long count(Request<InscriptionDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Inscription e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<InscriptionDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		InscriptionDto dto = request.getData() != null ? request.getData() : new InscriptionDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (InscriptionDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(InscriptionDto dto, HashMap<String, java.lang.Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getClasseId()!= null && dto.getClasseId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("classeId", dto.getClasseId(), "e.classe.id", "Integer", dto.getClasseIdParam(), param, index, locale));
			}
			if (dto.getEleveId()!= null && dto.getEleveId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("eleveId", dto.getEleveId(), "e.eleve.id", "Integer", dto.getEleveIdParam(), param, index, locale));
			}
			if (dto.getAnneeId()!= null && dto.getAnneeId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("anneeId", dto.getAnneeId(), "e.annee.id", "Integer", dto.getAnneeIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getClasseLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("classeLibelle", dto.getClasseLibelle(), "e.classe.libelle", "String", dto.getClasseLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getEleveNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("eleveNom", dto.getEleveNom(), "e.eleve.nom", "String", dto.getEleveNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getElevePrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("elevePrenom", dto.getElevePrenom(), "e.eleve.prenom", "String", dto.getElevePrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getAnneeLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("anneeLibelle", dto.getAnneeLibelle(), "e.annee.libelle", "String", dto.getAnneeLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
