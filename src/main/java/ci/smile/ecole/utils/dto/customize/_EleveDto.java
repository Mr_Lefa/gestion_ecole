
/*
 * Java dto for entity table eleve 
 * Created on 2021-01-18 ( Time 17:45:56 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.utils.dto.customize;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ci.smile.ecole.utils.contract.*;

/**
 * DTO customize for table "eleve"
 * 
 * @author Geo
 *
 */
@JsonInclude(Include.NON_NULL)
public class _EleveDto {
	
	private String image;

}
