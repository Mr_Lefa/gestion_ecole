



                                            										

/*
 * Java transformer for entity table note 
 * Created on 2021-01-18 ( Time 17:45:57 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.smile.ecole.dao.entity.Note;
import ci.smile.ecole.dao.repository.*;
import ci.smile.ecole.utils.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.*;
import ci.smile.ecole.utils.dto.transformer.*;
import ci.smile.ecole.utils.enums.*;
import ci.smile.ecole.dao.entity.Inscription;
import ci.smile.ecole.dao.entity.AttributionMatiere;
import ci.smile.ecole.dao.entity.*;

/**
BUSINESS for table "note"
 * 
 * @author Geo
 *
 */
@Log
@Component
public class NoteBusiness implements IBasicBusiness<Request<NoteDto>, Response<NoteDto>> {

	private Response<NoteDto> response;
	@Autowired
	private NoteRepository noteRepository;
//	@Autowired
//	private UserRepository userRepository;
	@Autowired
	private InscriptionRepository inscriptionRepository;
	
	@Autowired
	private MatiereRepository matiereRepository;
	
	
	@Autowired
	private AttributionMatiereRepository attributionMatiereRepository;

//    @Autowired
//    private UserBusiness userBusiness;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;


	@Autowired
	@Qualifier("mysqlJdbcTemplate")
	private JdbcTemplate mysqlTemplate;
	
	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

	public NoteBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	
	/**
	 * create Note by using NoteDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<NoteDto> create(Request<NoteDto> request, Locale locale)  {
		log.info("----begin create Note-----");
		
		response = new Response<NoteDto>();
		
		try {
//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.CREATE_NOTE.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}
			
			List<Note> items = new ArrayList<Note>();
			
			for (NoteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("libelle", dto.getLibelle());
				fieldsToVerify.put("attributionMatiereId", dto.getAttributionMatiereId());
				fieldsToVerify.put("inscriptionId", dto.getInscriptionId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if note to insert do not exist
				Note existingEntity = null;
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("note id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// verif unique libelle in db
				existingEntity = noteRepository.findByLibelle(dto.getLibelle(), false);
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("note libelle -> " + dto.getLibelle(), locale));
					response.setHasError(true);
					return response;
				}
				// verif unique libelle in items to save
				if (items.stream().anyMatch(a -> a.getLibelle().equalsIgnoreCase(dto.getLibelle()))) {
					response.setStatus(functionalError.DATA_DUPLICATE(" libelle ", locale));
					response.setHasError(true);
					return response;
				}

				// Verify if inscription exist
				Inscription existingInscription = null;
				if (dto.getInscriptionId() != null && dto.getInscriptionId() > 0){
					existingInscription = inscriptionRepository.findOne(dto.getInscriptionId(), false);
					if (existingInscription == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("inscription inscriptionId -> " + dto.getInscriptionId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				// Verify if attributionMatiere exist
				AttributionMatiere existingAttributionMatiere = null;
				if (dto.getAttributionMatiereId() != null && dto.getAttributionMatiereId() > 0){
					existingAttributionMatiere = attributionMatiereRepository.findOne(dto.getAttributionMatiereId(), false);
					if (existingAttributionMatiere == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("attributionMatiere attributionMatiereId -> " + dto.getAttributionMatiereId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				Note entityToSave = null;
				entityToSave = NoteTransformer.INSTANCE.toEntity(dto, existingInscription, existingAttributionMatiere);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Note> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = noteRepository.saveAll((Iterable<Note>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("note", locale));
					response.setHasError(true);
					return response;
				}
				List<NoteDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? NoteTransformer.INSTANCE.toLiteDtos(itemsSaved) : NoteTransformer.INSTANCE.toDtos(itemsSaved);
				
				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			log.info("----end create Note-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update Note by using NoteDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<NoteDto> update(Request<NoteDto> request, Locale locale)  {
		log.info("----begin update Note-----");
		
		response = new Response<NoteDto>();
		
		try {
//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.UPDATE_NOTE.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}

			List<Note> items = new ArrayList<Note>();
			
			for (NoteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la note existe
				Note entityToSave = null;
				entityToSave = noteRepository.findOne(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("note id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if inscription exist
				if (dto.getInscriptionId() != null && dto.getInscriptionId() > 0){
					Inscription existingInscription = inscriptionRepository.findOne(dto.getInscriptionId(), false);
					if (existingInscription == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("inscription inscriptionId -> " + dto.getInscriptionId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setInscription(existingInscription);
				}
				// Verify if attributionMatiere exist
				if (dto.getAttributionMatiereId() != null && dto.getAttributionMatiereId() > 0){
					AttributionMatiere existingAttributionMatiere = attributionMatiereRepository.findOne(dto.getAttributionMatiereId(), false);
					if (existingAttributionMatiere == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("attributionMatiere attributionMatiereId -> " + dto.getAttributionMatiereId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setAttributionMatiere(existingAttributionMatiere);
				}
				if (Utilities.notBlank(dto.getLibelle())) {
					entityToSave.setLibelle(dto.getLibelle());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<Note> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = noteRepository.saveAll((Iterable<Note>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("note", locale));
					response.setHasError(true);
					return response;
				}
				List<NoteDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? NoteTransformer.INSTANCE.toLiteDtos(itemsSaved) : NoteTransformer.INSTANCE.toDtos(itemsSaved);

				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			log.info("----end update Note-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete Note by using NoteDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<NoteDto> delete(Request<NoteDto> request, Locale locale)  {
		log.info("----begin delete Note-----");
		
		response = new Response<NoteDto>();
		
		try {
//
//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.DELETE_NOTE.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}

			List<Note> items = new ArrayList<Note>();
			
			for (NoteDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la note existe
				Note existingEntity = null;
				existingEntity = noteRepository.findOne(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("note -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------



				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				noteRepository.saveAll((Iterable<Note>) items);

				response.setHasError(false);
			}

			log.info("----end delete Note-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get Note by using NoteDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<NoteDto> getByCriteria(Request<NoteDto> request, Locale locale) {
		log.info("----begin get Note-----");
		
		response = new Response<NoteDto>();
		
		try {
//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.VIEW_NOTE.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}

			List<Note> items = null;
			items = noteRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<NoteDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? NoteTransformer.INSTANCE.toLiteDtos(items) : NoteTransformer.INSTANCE.toDtos(items);

				final int size = items.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);
				response.setCount(noteRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("note", locale));
				response.setHasError(false);
				return response;
			}

			log.info("----end get Note-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}
	
	
	/**Calcul de moyenne selon une 
	  *  Classe et pour un
	  *  Eleve specifique*/
	 @SuppressWarnings("unused")
	 @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	 public Response<NoteDto> moyenneByEleve(Request<NoteDto> request, Locale locale) {
	  log.info("----begin calculMatiereByEleve Eleve-----");

	  response = new Response<NoteDto>();
	  try {
	   List<Map<String, Object>> map = new ArrayList<>();
	   List<NoteDto> items = new ArrayList<NoteDto>();

	   for (NoteDto dto : request.getDatas()) {
		   
//			// Verifier si la MATIERE existe
//			Matiere existingEntity = null;
//			existingEntity = matiereRepository.findOne(dto.getId(), false);
//			if (existingEntity == null) {
//				response.setStatus(
//						functionalError.DATA_NOT_EXIST("la matiere n existe pas  -> " + dto.getId(), locale));
//				response.setHasError(true);
//				return response;
//			}
	     

	    String query ="SELECT AVG(note.libelle) AS moyenne ,eleve.nom AS nom ,eleve.prenom ,inscription.id AS inscription , matiere.libelle AS MATIERE ,annee.libelle AS annee ,classe.libelle AS class FROM note ,eleve ,inscription,matiere,attribution_matiere ,annee ,classe "
	    		+ " WHERE note.inscription_id = inscription.id "
	    		+ " AND inscription.eleve_id = eleve.id "
	    		+ " AND note.attribution_matiere_id = attribution_matiere.id "
	    		+ " AND matiere.id=attribution_matiere.matiere_id "
	    		+ " AND annee.id =inscription.annee_id "
	    		+ " AND classe.id=inscription.classe_id "
	    		+ " AND matiere.id='" + dto.getMatiereId()
	    		
	    		+ "' AND eleve.nom ='"+ dto.getEleveNom()+ "'; ";

	    map = mysqlTemplate.queryForList(query);

	    for (Map<String, Object> m : map) {
	     NoteDto noteDto = new NoteDto();
	     noteDto.setEleveNom(m.get("nom").toString());
	     noteDto.setMoyenne(m.get("moyenne").toString());

//	     noteDto.setElevePrenom(m.get("e_prenom").toString());
//	     noteDto.setMatiereLibelle(m.get("m_libelle").toString());
	     items.add(noteDto);
	    }
	    response.setStatus(functionalError.SUCCESS("Moyenne calculée", locale));
	    response.setItems(items);
	    response.setHasError(false);

	    
	   }

	   log.info("----end create Note-----");
	  } catch (PermissionDeniedDataAccessException e) {
	   exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
	  } catch (DataAccessResourceFailureException e) {
	   exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
	  } catch (DataAccessException e) {
	   exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
	  } catch (RuntimeException e) {
	   exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
	  } catch (Exception e) {
	   exceptionUtils.EXCEPTION(response, locale, e);
	  } finally {
	   if (response.isHasError() && response.getStatus() != null) {
	    log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(),
	      response.getStatus().getMessage()));
	    throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
	   }
	  }
	  return response;

	 }
	 
	 /**Calcul de moyenne selon une 
	  *  Classe et pour un
	  *  Eleve specifique*/
	 @SuppressWarnings("unused")
	 @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	 public Response<NoteDto> getEleveByClass(Request<NoteDto> request, Locale locale) {
	  log.info("----begin calculMatiereByEleve Eleve-----");

	  response = new Response<NoteDto>();
	  try {
	   List<Map<String, Object>> map = new ArrayList<>();
	   List<NoteDto> items = new ArrayList<NoteDto>();

	   for (NoteDto dto : request.getDatas()) {
		   
//			// Verifier si la MATIERE existe
//			Matiere existingEntity = null;
//			existingEntity = matiereRepository.findOne(dto.getId(), false);
//			if (existingEntity == null) {
//				response.setStatus(
//						functionalError.DATA_NOT_EXIST("la matiere n existe pas  -> " + dto.getId(), locale));
//				response.setHasError(true);
//				return response;
//			}
	     

	    String query ="SELECT AVG(note.libelle) AS moyenne ,eleve.nom AS nom ,eleve.prenom ,inscription.id AS inscription , matiere.libelle AS MATIERE ,annee.libelle AS annee ,classe.libelle AS class FROM note ,eleve ,inscription,matiere,attribution_matiere ,annee ,classe "
	    		+ " WHERE note.inscription_id = inscription.id "
	    		+ " AND inscription.eleve_id = eleve.id "
	    		+ " AND note.attribution_matiere_id = attribution_matiere.id "
	    		+ " AND matiere.id=attribution_matiere.matiere_id "
	    		+ " AND annee.id =inscription.annee_id "
	    		+ " AND classe.id=inscription.classe_id "
	    		+ " AND matiere.id='" + dto.getMatiereId()
	    		
	    		+ "' AND eleve.nom ='"+ dto.getEleveNom()+ "'; ";

	    map = mysqlTemplate.queryForList(query);

	    for (Map<String, Object> m : map) {
	     NoteDto noteDto = new NoteDto();
	     noteDto.setEleveNom(m.get("nom").toString());
	     noteDto.setMoyenne(m.get("moyenne").toString());

//	     noteDto.setElevePrenom(m.get("e_prenom").toString());
//	     noteDto.setMatiereLibelle(m.get("m_libelle").toString());
	     items.add(noteDto);
	    }
	    response.setStatus(functionalError.SUCCESS("Moyenne calculée", locale));
	    response.setItems(items);
	    response.setHasError(false);

	    
	   }

	   log.info("----end create Note-----");
	  } catch (PermissionDeniedDataAccessException e) {
	   exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
	  } catch (DataAccessResourceFailureException e) {
	   exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
	  } catch (DataAccessException e) {
	   exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
	  } catch (RuntimeException e) {
	   exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
	  } catch (Exception e) {
	   exceptionUtils.EXCEPTION(response, locale, e);
	  } finally {
	   if (response.isHasError() && response.getStatus() != null) {
	    log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(),
	      response.getStatus().getMessage()));
	    throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
	   }
	  }
	  return response;

	 }
	 
	 
	 /**Calcul de moyenne selon une 
	  *  Classe et pour un
	  *  Eleve specifique*/
	 @SuppressWarnings("unused")
	 @Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	 public Response<NoteDto> countNote(Request<NoteDto> request, Locale locale) {
	  log.info("----begin count Note-----");

	  response = new Response<NoteDto>();
	  try {
	   List<Map<String, Object>> map = new ArrayList<>();
	   List<NoteDto> items = new ArrayList<NoteDto>();

	   for (NoteDto dto : request.getDatas()) {
		   


	    String query ="SELECT COUNT(*) AS nombreNote FROM note ";

	    map = mysqlTemplate.queryForList(query);

	    for (Map<String, Object> m : map) {
	     NoteDto noteDto = new NoteDto();
	     noteDto.setNombreNote(m.get("nombreNote").toString());

//	     noteDto.setElevePrenom(m.get("e_prenom").toString());
//	     noteDto.setMatiereLibelle(m.get("m_libelle").toString());
	     items.add(noteDto);
	    }
	    response.setStatus(functionalError.SUCCESS("Nombre note", locale));
	    response.setItems(items);
	    response.setHasError(false);

	    
	   }

	   log.info("----end count Note-----");
	  } catch (PermissionDeniedDataAccessException e) {
	   exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
	  } catch (DataAccessResourceFailureException e) {
	   exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
	  } catch (DataAccessException e) {
	   exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
	  } catch (RuntimeException e) {
	   exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
	  } catch (Exception e) {
	   exceptionUtils.EXCEPTION(response, locale, e);
	  } finally {
	   if (response.isHasError() && response.getStatus() != null) {
	    log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(),
	      response.getStatus().getMessage()));
	    throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
	   }
	  }
	  return response;

	 }


	/**
	 * get full NoteDto by using Note as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private NoteDto getFullInfos(NoteDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

}
