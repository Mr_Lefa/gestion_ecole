package ci.smile.ecole.utils;

import org.springframework.stereotype.Component;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;



@Getter
@Component
public class ParamsUtils {
 @Value("${spring.datasource.url}")
  private String urlDb;
 
 @Value("${spring.datasource.username}")
  private String userDb;
 
 @Value("${spring.datasource.password}")
  private String passDb;
 
 @Value("${spring.datasource.driverClassName}")
  private String userDataSource;

}