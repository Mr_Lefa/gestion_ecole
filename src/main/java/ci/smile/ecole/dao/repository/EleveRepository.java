package ci.smile.ecole.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.smile.ecole.dao.entity.*;
import ci.smile.ecole.dao.repository.customize._EleveRepository;
import ci.smile.ecole.utils.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.*;

/**
 * Repository : Eleve.
 */
@Repository
public interface EleveRepository extends JpaRepository<Eleve, Integer>, _EleveRepository {
	/**
	 * Finds Eleve by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object Eleve whose id is equals to the given id. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.id = :id and e.isDeleted = :isDeleted")
	Eleve findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds Eleve by using matricule as a search criteria.
	 * 
	 * @param matricule
	 * @return An Object Eleve whose matricule is equals to the given matricule. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.matricule = :matricule and e.isDeleted = :isDeleted")
	Eleve findByMatricule(@Param("matricule")String matricule, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Eleve by using nom as a search criteria.
	 * 
	 * @param nom
	 * @return An Object Eleve whose nom is equals to the given nom. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.nom = :nom and e.isDeleted = :isDeleted")
	List<Eleve> findByNom(@Param("nom")String nom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Eleve by using prenom as a search criteria.
	 * 
	 * @param prenom
	 * @return An Object Eleve whose prenom is equals to the given prenom. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.prenom = :prenom and e.isDeleted = :isDeleted")
	List<Eleve> findByPrenom(@Param("prenom")String prenom, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Eleve by using sexe as a search criteria.
	 * 
	 * @param sexe
	 * @return An Object Eleve whose sexe is equals to the given sexe. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.sexe = :sexe and e.isDeleted = :isDeleted")
	List<Eleve> findBySexe(@Param("sexe")String sexe, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Eleve by using dateDeNaissance as a search criteria.
	 * 
	 * @param dateDeNaissance
	 * @return An Object Eleve whose dateDeNaissance is equals to the given dateDeNaissance. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.dateDeNaissance = :dateDeNaissance and e.isDeleted = :isDeleted")
	List<Eleve> findByDateDeNaissance(@Param("dateDeNaissance")Date dateDeNaissance, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Eleve by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object Eleve whose createdAt is equals to the given createdAt. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<Eleve> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Eleve by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object Eleve whose updatedAt is equals to the given updatedAt. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<Eleve> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Eleve by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object Eleve whose createdBy is equals to the given createdBy. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<Eleve> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Eleve by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object Eleve whose updatedBy is equals to the given updatedBy. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<Eleve> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds Eleve by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object Eleve whose isDeleted is equals to the given isDeleted. If
	 *         no Eleve is found, this method returns null.
	 */
	@Query("select e from Eleve e where e.isDeleted = :isDeleted")
	List<Eleve> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of Eleve by using eleveDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of Eleve
	 * @throws DataAccessException,ParseException
	 */
	default List<Eleve> getByCriteria(Request<EleveDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from Eleve e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<Eleve> query = em.createQuery(req, Eleve.class);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of Eleve by using eleveDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of Eleve
	 * 
	 */
	default Long count(Request<EleveDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from Eleve e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<EleveDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		EleveDto dto = request.getData() != null ? request.getData() : new EleveDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (EleveDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(EleveDto dto, HashMap<String, java.lang.Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMatricule())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("matricule", dto.getMatricule(), "e.matricule", "String", dto.getMatriculeParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getNom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("nom", dto.getNom(), "e.nom", "String", dto.getNomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getPrenom())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("prenom", dto.getPrenom(), "e.prenom", "String", dto.getPrenomParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getSexe())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("sexe", dto.getSexe(), "e.sexe", "String", dto.getSexeParam(), param, index, locale));
			}
			if (dto.getDateDeNaissance()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("dateDeNaissance", dto.getDateDeNaissance(), "e.dateDeNaissance", "Date", dto.getDateDeNaissanceParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
