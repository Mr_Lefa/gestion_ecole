



                                            										

/*
 * Java transformer for entity table attribution_matiere 
 * Created on 2021-01-18 ( Time 17:45:55 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.business;

import lombok.extern.java.Log;
import org.apache.commons.lang3.StringUtils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Collections;

import javax.persistence.PersistenceContext;
import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.PermissionDeniedDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import ci.smile.ecole.dao.entity.AttributionMatiere;
import ci.smile.ecole.dao.entity.Matiere;
import ci.smile.ecole.dao.repository.*;
import ci.smile.ecole.utils.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.*;
import ci.smile.ecole.utils.dto.transformer.*;
import ci.smile.ecole.utils.enums.*;
import ci.smile.ecole.dao.entity.Classe;
import ci.smile.ecole.dao.entity.*;

/**
BUSINESS for table "attribution_matiere"
 * 
 * @author Geo
 *
 */
@Log
@Component
public class AttributionMatiereBusiness implements IBasicBusiness<Request<AttributionMatiereDto>, Response<AttributionMatiereDto>> {

	private Response<AttributionMatiereDto> response;
	@Autowired
	private AttributionMatiereRepository attributionMatiereRepository;
//	@Autowired
//	private UserRepository userRepository;
	@Autowired
	private MatiereRepository matiereRepository;
	@Autowired
	private ClasseRepository classeRepository;
	@Autowired
	private NoteRepository noteRepository;

//    @Autowired
//    private UserBusiness userBusiness;

	@Autowired
	private FunctionalError functionalError;
	@Autowired
	private TechnicalError technicalError;
	@Autowired
	private ExceptionUtils exceptionUtils;
	@PersistenceContext
	private EntityManager em;

	private SimpleDateFormat dateFormat;
	private SimpleDateFormat dateTimeFormat;

	public AttributionMatiereBusiness() {
		dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		dateTimeFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	}

	
	/**
	 * create AttributionMatiere by using AttributionMatiereDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AttributionMatiereDto> create(Request<AttributionMatiereDto> request, Locale locale)  {
		log.info("----begin create AttributionMatiere-----");
		
		response = new Response<AttributionMatiereDto>();
		
		try {
//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.CREATE_ATTRIBUTION_MATIERE.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}
			
			List<AttributionMatiere> items = new ArrayList<AttributionMatiere>();
			
			for (AttributionMatiereDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("coefficient", dto.getCoefficient());
				fieldsToVerify.put("matiereId", dto.getMatiereId());
				fieldsToVerify.put("classeId", dto.getClasseId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if attributionMatiere to insert do not exist
				AttributionMatiere existingEntity = null;
				if (existingEntity != null) {
					response.setStatus(functionalError.DATA_EXIST("attributionMatiere id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if matiere exist
				Matiere existingMatiere = null;
				if (dto.getMatiereId() != null && dto.getMatiereId() > 0){
					existingMatiere = matiereRepository.findOne(dto.getMatiereId(), false);
					if (existingMatiere == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("matiere matiereId -> " + dto.getMatiereId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				// Verify if classe exist
				Classe existingClasse = null;
				if (dto.getClasseId() != null && dto.getClasseId() > 0){
					existingClasse = classeRepository.findOne(dto.getClasseId(), false);
					if (existingClasse == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("classe classeId -> " + dto.getClasseId(), locale));
						response.setHasError(true);
						return response;
					}
				}
				AttributionMatiere entityToSave = null;
				entityToSave = AttributionMatiereTransformer.INSTANCE.toEntity(dto, existingMatiere, existingClasse);
				entityToSave.setCreatedAt(Utilities.getCurrentDate());
				entityToSave.setCreatedBy(request.getUser());
				entityToSave.setIsDeleted(false);
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AttributionMatiere> itemsSaved = null;
				// inserer les donnees en base de donnees
				itemsSaved = attributionMatiereRepository.saveAll((Iterable<AttributionMatiere>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("attributionMatiere", locale));
					response.setHasError(true);
					return response;
				}
				List<AttributionMatiereDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? AttributionMatiereTransformer.INSTANCE.toLiteDtos(itemsSaved) : AttributionMatiereTransformer.INSTANCE.toDtos(itemsSaved);
				
				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			log.info("----end create AttributionMatiere-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * update AttributionMatiere by using AttributionMatiereDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AttributionMatiereDto> update(Request<AttributionMatiereDto> request, Locale locale)  {
		log.info("----begin update AttributionMatiere-----");
		
		response = new Response<AttributionMatiereDto>();
		
		try {
//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.UPDATE_ATTRIBUTION_MATIERE.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}

			List<AttributionMatiere> items = new ArrayList<AttributionMatiere>();
			
			for (AttributionMatiereDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la attributionMatiere existe
				AttributionMatiere entityToSave = null;
				entityToSave = attributionMatiereRepository.findOne(dto.getId(), false);
				if (entityToSave == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("attributionMatiere id -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// Verify if matiere exist
				if (dto.getMatiereId() != null && dto.getMatiereId() > 0){
					Matiere existingMatiere = matiereRepository.findOne(dto.getMatiereId(), false);
					if (existingMatiere == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("matiere matiereId -> " + dto.getMatiereId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setMatiere(existingMatiere);
				}
				// Verify if classe exist
				if (dto.getClasseId() != null && dto.getClasseId() > 0){
					Classe existingClasse = classeRepository.findOne(dto.getClasseId(), false);
					if (existingClasse == null) {
						response.setStatus(functionalError.DATA_NOT_EXIST("classe classeId -> " + dto.getClasseId(), locale));
						response.setHasError(true);
						return response;
					}
					entityToSave.setClasse(existingClasse);
				}
				if (dto.getCoefficient() != null && dto.getCoefficient() > 0) {
					entityToSave.setCoefficient(dto.getCoefficient());
				}
				if (dto.getCreatedBy() != null && dto.getCreatedBy() > 0) {
					entityToSave.setCreatedBy(dto.getCreatedBy());
				}
				if (dto.getUpdatedBy() != null && dto.getUpdatedBy() > 0) {
					entityToSave.setUpdatedBy(dto.getUpdatedBy());
				}
				entityToSave.setUpdatedAt(Utilities.getCurrentDate());
				entityToSave.setUpdatedBy(request.getUser());
				items.add(entityToSave);
			}

			if (!items.isEmpty()) {
				List<AttributionMatiere> itemsSaved = null;
				// maj les donnees en base
				itemsSaved = attributionMatiereRepository.saveAll((Iterable<AttributionMatiere>) items);
				if (itemsSaved == null) {
					response.setStatus(functionalError.SAVE_FAIL("attributionMatiere", locale));
					response.setHasError(true);
					return response;
				}
				List<AttributionMatiereDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? AttributionMatiereTransformer.INSTANCE.toLiteDtos(itemsSaved) : AttributionMatiereTransformer.INSTANCE.toDtos(itemsSaved);

				final int size = itemsSaved.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);
				response.setHasError(false);
			}

			log.info("----end update AttributionMatiere-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * delete AttributionMatiere by using AttributionMatiereDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Transactional(rollbackFor = { RuntimeException.class, Exception.class })
	@Override
	public Response<AttributionMatiereDto> delete(Request<AttributionMatiereDto> request, Locale locale)  {
		log.info("----begin delete AttributionMatiere-----");
		
		response = new Response<AttributionMatiereDto>();
		
		try {

//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.DELETE_ATTRIBUTION_MATIERE.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}

			List<AttributionMatiere> items = new ArrayList<AttributionMatiere>();
			
			for (AttributionMatiereDto dto : request.getDatas()) {
				// Definir les parametres obligatoires
				Map<String, java.lang.Object> fieldsToVerify = new HashMap<String, java.lang.Object>();
				fieldsToVerify.put("id", dto.getId());
				if (!Validate.RequiredValue(fieldsToVerify).isGood()) {
					response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
					response.setHasError(true);
					return response;
				}

				// Verifier si la attributionMatiere existe
				AttributionMatiere existingEntity = null;
				existingEntity = attributionMatiereRepository.findOne(dto.getId(), false);
				if (existingEntity == null) {
					response.setStatus(functionalError.DATA_NOT_EXIST("attributionMatiere -> " + dto.getId(), locale));
					response.setHasError(true);
					return response;
				}

				// -----------------------------------------------------------------------
				// ----------- CHECK IF DATA IS USED
				// -----------------------------------------------------------------------

				// note
				List<Note> listOfNote = noteRepository.findByAttributionMatiereId(existingEntity.getId(), false);
				if (listOfNote != null && !listOfNote.isEmpty()){
					response.setStatus(functionalError.DATA_NOT_DELETABLE("(" + listOfNote.size() + ")", locale));
					response.setHasError(true);
					return response;
				}


				existingEntity.setIsDeleted(true);
				items.add(existingEntity);
			}

			if (!items.isEmpty()) {
				// supprimer les donnees en base
				attributionMatiereRepository.saveAll((Iterable<AttributionMatiere>) items);

				response.setHasError(false);
			}

			log.info("----end delete AttributionMatiere-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get AttributionMatiere by using AttributionMatiereDto as object.
	 * 
	 * @param request
	 * @return response
	 * 
	 */
	@SuppressWarnings("unused")
	@Override
	public Response<AttributionMatiereDto> getByCriteria(Request<AttributionMatiereDto> request, Locale locale) {
		log.info("----begin get AttributionMatiere-----");
		
		response = new Response<AttributionMatiereDto>();
		
		try {
//			Map<String, java.lang.Object> fieldsToVerifyUser = new HashMap<String, java.lang.Object>();
//			fieldsToVerifyUser.put("user", request.getUser());
//			if (!Validate.RequiredValue(fieldsToVerifyUser).isGood()) {
//				response.setStatus(functionalError.FIELD_EMPTY(Validate.getValidate().getField(), locale));
//				response.setHasError(true);
//				return response;
//			}

//			Response<UserDto> userResponse = userBusiness.isGranted(request.getUser(), FunctionalityEnum.VIEW_ATTRIBUTION_MATIERE.getValue(), locale);
//			if (userResponse.isHasError()) {
//				response.setHasError(true);
//				response.setStatus(userResponse.getStatus());
//				return response;
//			}

			List<AttributionMatiere> items = null;
			items = attributionMatiereRepository.getByCriteria(request, em, locale);
			if (items != null && !items.isEmpty()) {
				List<AttributionMatiereDto> itemsDto = (Utilities.isTrue(request.getIsSimpleLoading())) ? AttributionMatiereTransformer.INSTANCE.toLiteDtos(items) : AttributionMatiereTransformer.INSTANCE.toDtos(items);

				final int size = items.size();
				List<String>  listOfError      = Collections.synchronizedList(new ArrayList<String>());
				itemsDto.parallelStream().forEach(dto -> {
					try {
						dto = getFullInfos(dto, size, request.getIsSimpleLoading(), locale);
					} catch (Exception e) {
						listOfError.add(e.getMessage());
						e.printStackTrace();
					}
				});
				if (Utilities.isNotEmpty(listOfError)) {
					Object[] objArray = listOfError.stream().distinct().toArray();
					throw new RuntimeException(StringUtils.join(objArray, ", "));
				}
				response.setItems(itemsDto);
				response.setCount(attributionMatiereRepository.count(request, em, locale));
				response.setHasError(false);
			} else {
				response.setStatus(functionalError.DATA_EMPTY("attributionMatiere", locale));
				response.setHasError(false);
				return response;
			}

			log.info("----end get AttributionMatiere-----");
		} catch (PermissionDeniedDataAccessException e) {
			exceptionUtils.PERMISSION_DENIED_DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (DataAccessResourceFailureException e) {
			exceptionUtils.DATA_ACCESS_RESOURCE_FAILURE_EXCEPTION(response, locale, e);
		} catch (DataAccessException e) {
			exceptionUtils.DATA_ACCESS_EXCEPTION(response, locale, e);
		} catch (RuntimeException e) {
			exceptionUtils.RUNTIME_EXCEPTION(response, locale, e);
		} catch (Exception e) {
			exceptionUtils.EXCEPTION(response, locale, e);
		} finally {
			if (response.isHasError() && response.getStatus() != null) {
				log.info(String.format("Erreur| code: {} -  message: {}", response.getStatus().getCode(), response.getStatus().getMessage()));
				throw new RuntimeException(response.getStatus().getCode() + ";" + response.getStatus().getMessage());
			}
		}
		return response;
	}

	/**
	 * get full AttributionMatiereDto by using AttributionMatiere as object.
	 * 
	 * @param dto
	 * @param size
	 * @param isSimpleLoading
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	private AttributionMatiereDto getFullInfos(AttributionMatiereDto dto, Integer size, Boolean isSimpleLoading, Locale locale) throws Exception {
		// put code here

		if (Utilities.isTrue(isSimpleLoading)) {
			return dto;
		}
		if (size > 1) {
			return dto;
		}

		return dto;
	}

}
