
/*
 * Java dto for entity table note 
 * Created on 2021-01-15 ( Time 15:14:46 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.utils.dto.customize;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Data;

/**
 * DTO customize for table "note"
 * 
 * @author Geo
 *
 */
@Data
@JsonInclude(Include.NON_NULL)
public class _NoteDto {

//	 private String eleveNom;
//	 private String elevePrenom;
//	 private String matiereLibelle;
//	 private String moyenneMatiere; 
//	 private String classeLibelle;
//	 private String moyenneGenerale;
//	 private String eleveAgeString;
//	 private String classementGenerale;
	
	 private String noteInscriptionId;
	 private String inscriptionEleveId;
	 private String noteAttributionMatiereId;
	 private String attributionMatiereMatiereId;
	 private String inscriptionMatiereId;
	 private String inscriptionAnneId;
	 private String inscriptionClasseId;
	 private String matiereId;
	 private String eleveNom;
	 private String moyenne;
	 private String nombreNote;



	
}
