package ci.smile.ecole.utils.report;

import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map;

//import ci.smile.assistanceAutoEtPro.utils.dto.UserDto;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

public class ReportUtils {

 public static String enteteImgUrl_1 = "reports/images/nav_avatar.png";
 public static String enteteImgUrl_3 = "reports/images/nav_favoris_stars.png";

 public static String enteteImgUrl_2 = "reports/images/nav_tag_color_event.png";
 public static String enteteImgUrl_4 = "reports/images/orange_detente.png";

 public static String enteteImgUrl_5 = "reports/images/map.jpg";

 public static JasperPrint getJasperPrint(String reportUrl, Map<String, Object> parameters, Collection dataSource)
   throws JRException {
  JasperPrint jasperPrint;

  try {
   InputStream resourceAsStream = Thread.currentThread().getContextClassLoader()
     .getResourceAsStream(reportUrl);
   // get report file and then load into jasperDesign
   JasperDesign jasperDesign = JRXmlLoader.load(resourceAsStream);

   // compile the jasperDesign
   JasperReport jasperReport = JasperCompileManager.compileReport(jasperDesign);

   // fill the ready report with data and parameter
   jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,
     new JRBeanCollectionDataSource(dataSource));

  } catch (JRException e) {
   // TODO: handle exception
   throw e;
  }

  return jasperPrint;
 }

 public static File getNewFile(String path) {
  return new File(getAbsoluePath(path));
 }

 public static boolean existFile(String path) {
  File file = getNewFile(path);
  return file.exists();
 }

 public static boolean existFiles(String path) {
  File file = new File(path);
  return file.exists();
 }

 public static String getAbsoluePath(String path) {
  String fullPath = path;
  Object object = Thread.currentThread().getContextClassLoader().getResource(path);
  if (object != null) {
   fullPath = Thread.currentThread().getContextClassLoader().getResource(path).getFile();
  }

  return fullPath;
 }

// public static void ajoutBase64ImagesScs(UserDto dto) {
//  if (ReportUtils.existFile(enteteImgUrl_1)) {
//   String entete = PicturesUtilities.convertToBase64(enteteImgUrl_1);
//   if (entete != null) {
//    dto.setImgUrl1(entete);
//   }
//  }
//  if (ReportUtils.existFile(enteteImgUrl_2)) {
//   String entete = PicturesUtilities.convertToBase64(enteteImgUrl_2);
//   if (entete != null) {
//    dto.setImgUrl2(entete);
//   }
//  }
//  if (ReportUtils.existFile(enteteImgUrl_3)) {
//   String entete = PicturesUtilities.convertToBase64(enteteImgUrl_3);
//   if (entete != null) {
//    dto.setImgUrl3(entete);
//   }
//  }
//  if (ReportUtils.existFile(enteteImgUrl_4)) {
//   String entete = PicturesUtilities.convertToBase64(enteteImgUrl_4);
//   if (entete != null) {
//    dto.setImgUrl4(entete);
//   }
//  }
//
//  if (ReportUtils.existFile(enteteImgUrl_5)) {
//   String entete = PicturesUtilities.convertToBase64(enteteImgUrl_5);
//   if (entete != null) {
//    dto.setImgUrl5(entete);
//   }
//  }
// }

// public static void ajoutBase64ImagesForTicket(EleveDto dto, String documentPath) {
//  if (ReportUtils.existFiles(documentPath)) {
//   String entete = PicturesUtilities.convertToBase64ForTest(documentPath);
//   if (entete != null) {
//    dto.setDocumentPath(entete);
//   }
//  }
// }

}