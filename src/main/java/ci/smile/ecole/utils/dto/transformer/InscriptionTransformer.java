

/*
 * Java transformer for entity table inscription 
 * Created on 2021-01-18 ( Time 17:45:56 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.utils.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.smile.ecole.dao.entity.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.*;


/**
 * TRANSFORMER for table "inscription"
 * 
 * @author Geo
 *
 */
@Mapper
public interface InscriptionTransformer {

	InscriptionTransformer INSTANCE = Mappers.getMapper(InscriptionTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.classe.id", target="classeId"),
		@Mapping(source="entity.classe.libelle", target="classeLibelle"),
		@Mapping(source="entity.eleve.id", target="eleveId"),
		@Mapping(source="entity.eleve.nom", target="eleveNom"),
		@Mapping(source="entity.eleve.prenom", target="elevePrenom"),
		@Mapping(source="entity.annee.id", target="anneeId"),
		@Mapping(source="entity.annee.libelle", target="anneeLibelle"),
	})
	InscriptionDto toDto(Inscription entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<InscriptionDto> toDtos(List<Inscription> entities) throws ParseException;

    default InscriptionDto toLiteDto(Inscription entity) {
		if (entity == null) {
			return null;
		}
		InscriptionDto dto = new InscriptionDto();
		dto.setId( entity.getId() );
		return dto;
    }

	default List<InscriptionDto> toLiteDtos(List<Inscription> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<InscriptionDto> dtos = new ArrayList<InscriptionDto>();
		for (Inscription entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="classe", target="classe"),
		@Mapping(source="eleve", target="eleve"),
		@Mapping(source="annee", target="annee"),
	})
    Inscription toEntity(InscriptionDto dto, Classe classe, Eleve eleve, Annee annee) throws ParseException;

    //List<Inscription> toEntities(List<InscriptionDto> dtos) throws ParseException;

}
