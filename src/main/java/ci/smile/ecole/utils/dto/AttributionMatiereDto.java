
/*
 * Java dto for entity table attribution_matiere 
 * Created on 2021-01-18 ( Time 17:45:55 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.utils.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.customize._AttributionMatiereDto;

/**
 * DTO for table "attribution_matiere"
 *
 * @author Geo
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class AttributionMatiereDto extends _AttributionMatiereDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private Integer    coefficient          ;
    private Integer    matiereId            ;
    private Integer    classeId             ;
	private String     createdAt            ;
	private String     updatedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Boolean    isDeleted            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String matiereLibelle;
	private String classeLibelle;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  coefficientParam      ;                     
	private SearchParam<Integer>  matiereIdParam        ;                     
	private SearchParam<Integer>  classeIdParam         ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   matiereLibelleParam   ;                     
	private SearchParam<String>   classeLibelleParam    ;                     
    /**
     * Default constructor
     */
    public AttributionMatiereDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
