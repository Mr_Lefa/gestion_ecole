package ci.smile.ecole.dao.repository;

import java.util.Date;
import java.util.List;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ci.smile.ecole.dao.entity.*;
import ci.smile.ecole.dao.repository.customize._AttributionMatiereRepository;
import ci.smile.ecole.utils.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.*;

/**
 * Repository : AttributionMatiere.
 */
@Repository
public interface AttributionMatiereRepository extends JpaRepository<AttributionMatiere, Integer>, _AttributionMatiereRepository {
	/**
	 * Finds AttributionMatiere by using id as a search criteria.
	 * 
	 * @param id
	 * @return An Object AttributionMatiere whose id is equals to the given id. If
	 *         no AttributionMatiere is found, this method returns null.
	 */
	@Query("select e from AttributionMatiere e where e.id = :id and e.isDeleted = :isDeleted")
	AttributionMatiere findOne(@Param("id")Integer id, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AttributionMatiere by using coefficient as a search criteria.
	 * 
	 * @param coefficient
	 * @return An Object AttributionMatiere whose coefficient is equals to the given coefficient. If
	 *         no AttributionMatiere is found, this method returns null.
	 */
	@Query("select e from AttributionMatiere e where e.coefficient = :coefficient and e.isDeleted = :isDeleted")
	List<AttributionMatiere> findByCoefficient(@Param("coefficient")Integer coefficient, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AttributionMatiere by using createdAt as a search criteria.
	 * 
	 * @param createdAt
	 * @return An Object AttributionMatiere whose createdAt is equals to the given createdAt. If
	 *         no AttributionMatiere is found, this method returns null.
	 */
	@Query("select e from AttributionMatiere e where e.createdAt = :createdAt and e.isDeleted = :isDeleted")
	List<AttributionMatiere> findByCreatedAt(@Param("createdAt")Date createdAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AttributionMatiere by using updatedAt as a search criteria.
	 * 
	 * @param updatedAt
	 * @return An Object AttributionMatiere whose updatedAt is equals to the given updatedAt. If
	 *         no AttributionMatiere is found, this method returns null.
	 */
	@Query("select e from AttributionMatiere e where e.updatedAt = :updatedAt and e.isDeleted = :isDeleted")
	List<AttributionMatiere> findByUpdatedAt(@Param("updatedAt")Date updatedAt, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AttributionMatiere by using createdBy as a search criteria.
	 * 
	 * @param createdBy
	 * @return An Object AttributionMatiere whose createdBy is equals to the given createdBy. If
	 *         no AttributionMatiere is found, this method returns null.
	 */
	@Query("select e from AttributionMatiere e where e.createdBy = :createdBy and e.isDeleted = :isDeleted")
	List<AttributionMatiere> findByCreatedBy(@Param("createdBy")Integer createdBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AttributionMatiere by using updatedBy as a search criteria.
	 * 
	 * @param updatedBy
	 * @return An Object AttributionMatiere whose updatedBy is equals to the given updatedBy. If
	 *         no AttributionMatiere is found, this method returns null.
	 */
	@Query("select e from AttributionMatiere e where e.updatedBy = :updatedBy and e.isDeleted = :isDeleted")
	List<AttributionMatiere> findByUpdatedBy(@Param("updatedBy")Integer updatedBy, @Param("isDeleted")Boolean isDeleted);
	/**
	 * Finds AttributionMatiere by using isDeleted as a search criteria.
	 * 
	 * @param isDeleted
	 * @return An Object AttributionMatiere whose isDeleted is equals to the given isDeleted. If
	 *         no AttributionMatiere is found, this method returns null.
	 */
	@Query("select e from AttributionMatiere e where e.isDeleted = :isDeleted")
	List<AttributionMatiere> findByIsDeleted(@Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AttributionMatiere by using matiereId as a search criteria.
	 * 
	 * @param matiereId
	 * @return An Object AttributionMatiere whose matiereId is equals to the given matiereId. If
	 *         no AttributionMatiere is found, this method returns null.
	 */
	@Query("select e from AttributionMatiere e where e.matiere.id = :matiereId and e.isDeleted = :isDeleted")
	List<AttributionMatiere> findByMatiereId(@Param("matiereId")Integer matiereId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds AttributionMatiere by using classeId as a search criteria.
	 * 
	 * @param classeId
	 * @return An Object AttributionMatiere whose classeId is equals to the given classeId. If
	 *         no AttributionMatiere is found, this method returns null.
	 */
	@Query("select e from AttributionMatiere e where e.classe.id = :classeId and e.isDeleted = :isDeleted")
	List<AttributionMatiere> findByClasseId(@Param("classeId")Integer classeId, @Param("isDeleted")Boolean isDeleted);

	/**
	 * Finds List of AttributionMatiere by using attributionMatiereDto as a search criteria.
	 * 
	 * @param request, em
	 * @return A List of AttributionMatiere
	 * @throws DataAccessException,ParseException
	 */
	default List<AttributionMatiere> getByCriteria(Request<AttributionMatiereDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception {
		String req = "select e from AttributionMatiere e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by e.id desc";
		TypedQuery<AttributionMatiere> query = em.createQuery(req, AttributionMatiere.class);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		if (request.getIndex() != null && request.getSize() != null) {
			query.setFirstResult(request.getIndex() * request.getSize());
			query.setMaxResults(request.getSize());
		}
		return query.getResultList();
	}

	/**
	 * Finds count of AttributionMatiere by using attributionMatiereDto as a search criteria.
	 * 
	 * @param request, em
	 * @return Number of AttributionMatiere
	 * 
	 */
	default Long count(Request<AttributionMatiereDto> request, EntityManager em, Locale locale) throws DataAccessException, Exception  {
		String req = "select count(e.id) from AttributionMatiere e where e IS NOT NULL";
		HashMap<String, java.lang.Object> param = new HashMap<String, java.lang.Object>();
		req += getWhereExpression(request, param, locale);
		req += " order by  e.id desc";
		javax.persistence.Query query = em.createQuery(req);
		for (Map.Entry<String, java.lang.Object> entry : param.entrySet()) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		Long count = (Long) query.getResultList().get(0);
		return count;
	}

	/**
	 * get where expression
	 * @param request
	 * @param param
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String getWhereExpression(Request<AttributionMatiereDto> request, HashMap<String, java.lang.Object> param, Locale locale) throws Exception {
		// main query
		AttributionMatiereDto dto = request.getData() != null ? request.getData() : new AttributionMatiereDto();
		dto.setIsDeleted(false);
		String mainReq = generateCriteria(dto, param, 0, locale);
		// others query
		String othersReq = "";
		if (request.getDatas() != null && !request.getDatas().isEmpty()) {
			Integer index = 1;
			for (AttributionMatiereDto elt : request.getDatas()) {
				elt.setIsDeleted(false);
				String eltReq = generateCriteria(elt, param, index, locale);
				if (request.getIsAnd() != null && request.getIsAnd()) {
					othersReq += "and (" + eltReq + ") ";
				} else {
					othersReq += "or (" + eltReq + ") ";
				}
				index++;
			}
		}
		String req = "";
		if (!mainReq.isEmpty()) {
			req += " and (" + mainReq + ") ";
		}
		req += othersReq;
		return req;
	}

	/**
	 * generate sql query for dto
	 * @param dto
	 * @param param
	 * @param index
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	default String generateCriteria(AttributionMatiereDto dto, HashMap<String, java.lang.Object> param, Integer index,  Locale locale) throws Exception{
		List<String> listOfQuery = new ArrayList<String>();
		if (dto != null) {
			if (dto.getId()!= null && dto.getId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("id", dto.getId(), "e.id", "Integer", dto.getIdParam(), param, index, locale));
			}
			if (dto.getCoefficient()!= null && dto.getCoefficient() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("coefficient", dto.getCoefficient(), "e.coefficient", "Integer", dto.getCoefficientParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getCreatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdAt", dto.getCreatedAt(), "e.createdAt", "Date", dto.getCreatedAtParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getUpdatedAt())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedAt", dto.getUpdatedAt(), "e.updatedAt", "Date", dto.getUpdatedAtParam(), param, index, locale));
			}
			if (dto.getCreatedBy()!= null && dto.getCreatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("createdBy", dto.getCreatedBy(), "e.createdBy", "Integer", dto.getCreatedByParam(), param, index, locale));
			}
			if (dto.getUpdatedBy()!= null && dto.getUpdatedBy() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("updatedBy", dto.getUpdatedBy(), "e.updatedBy", "Integer", dto.getUpdatedByParam(), param, index, locale));
			}
			if (dto.getIsDeleted()!= null) {
				listOfQuery.add(CriteriaUtils.generateCriteria("isDeleted", dto.getIsDeleted(), "e.isDeleted", "Boolean", dto.getIsDeletedParam(), param, index, locale));
			}
			if (dto.getMatiereId()!= null && dto.getMatiereId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("matiereId", dto.getMatiereId(), "e.matiere.id", "Integer", dto.getMatiereIdParam(), param, index, locale));
			}
			if (dto.getClasseId()!= null && dto.getClasseId() > 0) {
				listOfQuery.add(CriteriaUtils.generateCriteria("classeId", dto.getClasseId(), "e.classe.id", "Integer", dto.getClasseIdParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getMatiereLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("matiereLibelle", dto.getMatiereLibelle(), "e.matiere.libelle", "String", dto.getMatiereLibelleParam(), param, index, locale));
			}
			if (Utilities.notBlank(dto.getClasseLibelle())) {
				listOfQuery.add(CriteriaUtils.generateCriteria("classeLibelle", dto.getClasseLibelle(), "e.classe.libelle", "String", dto.getClasseLibelleParam(), param, index, locale));
			}
			List<String> listOfCustomQuery = _generateCriteria(dto, param, index, locale);
			if (Utilities.isNotEmpty(listOfCustomQuery)) {
				listOfQuery.addAll(listOfCustomQuery);
			}
		}
		return CriteriaUtils.getCriteriaByListOfQuery(listOfQuery);
	}
}
