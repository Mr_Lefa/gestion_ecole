
/*
 * Java dto for entity table inscription 
 * Created on 2021-01-18 ( Time 17:45:56 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.utils.dto;

import java.util.Date;
import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.customize._InscriptionDto;

/**
 * DTO for table "inscription"
 *
 * @author Geo
 */
@Data
@ToString
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(alphabetic = true)
public class InscriptionDto extends _InscriptionDto implements Cloneable{

    private Integer    id                   ; // Primary Key

    private Integer    eleveId              ;
    private Integer    anneeId              ;
    private Integer    classeId             ;
	private String     createdAt            ;
	private String     updatedAt            ;
    private Integer    createdBy            ;
    private Integer    updatedBy            ;
    private Boolean    isDeleted            ;

    //----------------------------------------------------------------------
    // ENTITY LINKS FIELD ( RELATIONSHIP )
    //----------------------------------------------------------------------
	private String classeLibelle;
	private String eleveNom;
	private String elevePrenom;
	private String anneeLibelle;

	// Search param
	private SearchParam<Integer>  idParam               ;                     
	private SearchParam<Integer>  eleveIdParam          ;                     
	private SearchParam<Integer>  anneeIdParam          ;                     
	private SearchParam<Integer>  classeIdParam         ;                     
	private SearchParam<String>   createdAtParam        ;                     
	private SearchParam<String>   updatedAtParam        ;                     
	private SearchParam<Integer>  createdByParam        ;                     
	private SearchParam<Integer>  updatedByParam        ;                     
	private SearchParam<Boolean>  isDeletedParam        ;                     
	private SearchParam<String>   classeLibelleParam    ;                     
	private SearchParam<String>   eleveNomParam         ;                     
	private SearchParam<String>   elevePrenomParam      ;                     
	private SearchParam<String>   anneeLibelleParam     ;                     
    /**
     * Default constructor
     */
    public InscriptionDto()
    {
        super();
    }
    
	//----------------------------------------------------------------------
    // clone METHOD
    //----------------------------------------------------------------------
	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
}
