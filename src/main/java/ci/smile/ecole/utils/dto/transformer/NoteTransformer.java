

/*
 * Java transformer for entity table note 
 * Created on 2021-01-18 ( Time 17:45:57 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.utils.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.smile.ecole.dao.entity.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.*;


/**
 * TRANSFORMER for table "note"
 * 
 * @author Geo
 *
 */
@Mapper
public interface NoteTransformer {

	NoteTransformer INSTANCE = Mappers.getMapper(NoteTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="entity.inscription.id", target="inscriptionId"),
		@Mapping(source="entity.attributionMatiere.id", target="attributionMatiereId"),
	})
	NoteDto toDto(Note entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<NoteDto> toDtos(List<Note> entities) throws ParseException;

    default NoteDto toLiteDto(Note entity) {
		if (entity == null) {
			return null;
		}
		NoteDto dto = new NoteDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	default List<NoteDto> toLiteDtos(List<Note> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<NoteDto> dtos = new ArrayList<NoteDto>();
		for (Note entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
		@Mapping(source="inscription", target="inscription"),
		@Mapping(source="attributionMatiere", target="attributionMatiere"),
	})
    Note toEntity(NoteDto dto, Inscription inscription, AttributionMatiere attributionMatiere) throws ParseException;

    //List<Note> toEntities(List<NoteDto> dtos) throws ParseException;

}
