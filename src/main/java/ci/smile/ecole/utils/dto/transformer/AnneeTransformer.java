

/*
 * Java transformer for entity table annee 
 * Created on 2021-01-18 ( Time 17:45:55 )
 * Generator tool : Telosys Tools Generator ( version 3.1.2 )
 * Copyright 2018 Geo. All Rights Reserved.
 */

package ci.smile.ecole.utils.dto.transformer;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;

import ci.smile.ecole.dao.entity.*;
import ci.smile.ecole.utils.contract.*;
import ci.smile.ecole.utils.dto.*;


/**
 * TRANSFORMER for table "annee"
 * 
 * @author Geo
 *
 */
@Mapper
public interface AnneeTransformer {

	AnneeTransformer INSTANCE = Mappers.getMapper(AnneeTransformer.class);

	@FullTransformerQualifier
	@Mappings({
		@Mapping(source="entity.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="entity.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
	})
	AnneeDto toDto(Annee entity) throws ParseException;

	@IterableMapping(qualifiedBy = {FullTransformerQualifier.class})
    List<AnneeDto> toDtos(List<Annee> entities) throws ParseException;

    default AnneeDto toLiteDto(Annee entity) {
		if (entity == null) {
			return null;
		}
		AnneeDto dto = new AnneeDto();
		dto.setId( entity.getId() );
		dto.setLibelle( entity.getLibelle() );
		return dto;
    }

	default List<AnneeDto> toLiteDtos(List<Annee> entities) {
		if (entities == null || entities.stream().allMatch(o -> o == null)) {
			return null;
		}
		List<AnneeDto> dtos = new ArrayList<AnneeDto>();
		for (Annee entity : entities) {
			dtos.add(toLiteDto(entity));
		}
		return dtos;
	}

	@Mappings({
		@Mapping(source="dto.id", target="id"),
		@Mapping(source="dto.libelle", target="libelle"),
		@Mapping(source="dto.createdAt", dateFormat="dd/MM/yyyy",target="createdAt"),
		@Mapping(source="dto.updatedAt", dateFormat="dd/MM/yyyy",target="updatedAt"),
		@Mapping(source="dto.createdBy", target="createdBy"),
		@Mapping(source="dto.updatedBy", target="updatedBy"),
		@Mapping(source="dto.isDeleted", target="isDeleted"),
	})
    Annee toEntity(AnneeDto dto) throws ParseException;

    //List<Annee> toEntities(List<AnneeDto> dtos) throws ParseException;

}
